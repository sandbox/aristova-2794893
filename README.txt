Slack Messages Block allows to create block which contain messages from the slack channel.

How to use
1. In the block page (admin/structure/block) place block Slack Messages at the appropriate theme region.
2. In the block configuration form set slack token (https://get.slack.help/hc/en-us/articles/215770388-Creating-and-regenerating-API-tokens), name  of slack channel (the channel which messages you want to show), number of messages to display.
3. Besides, the module displays username of author of message, his/her picture and date/time of message posting. In the block configuration form you can alter date/time format and picture's style.
4. The module provides a template. You can theme it you'll want to. 
