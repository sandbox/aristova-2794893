<?php

/**
 * @file
 * Contains Drupal\slack_messages_block\Plugin\Block\SlackMessagesBlock.
 */

namespace Drupal\slack_messages_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\Entity\DateFormat;

/**
 * @Block(
 *   id = "slack_messages",
 *   admin_label = @Translation("Slack messages"),
 * )
 */
class SlackMessagesBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'amount_messages' => 10,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $date_formats = DateFormat::loadMultiple();
    $date_formats_options = array();
    foreach ($date_formats as $name => $format) {
      $time = time();
      $date_formats_options[$name] = $format->label() . " - " . \Drupal::service('date.formatter')->format($time, $format->getPattern());
    }

    $form['slack_token'] = [
      '#type' => 'textfield',
      '#title' => t('Slack token'),
      '#default_value' => isset($this->configuration['slack_token']) ? $this->configuration['slack_token'] : "",
      '#required' => TRUE,

    ];

    $form['slack_channel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Insert name of channel without #'),
      '#default_value' => isset($this->configuration['slack_channel']) ? $this->configuration['slack_channel'] : "",
      '#required' => TRUE,
    ];

    $form['amount_messages'] = [
      '#type' => 'number',
      '#min' => 1,
      '#title' => t('How many messages will display in the block'),
      '#default_value' => isset($this->configuration['amount_messages']) ? $this->configuration['amount_messages'] : 10,
      '#required' => TRUE,
    ];


    $form['date_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Select date format'),
      '#options' => $date_formats_options,
      '#default_value' => isset($this->configuration['date_format']) ? $this->configuration['date_format'] : 'short',
      '#required' => TRUE,
    ];

    $form['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Select style for avatar display'),
      '#options' => image_style_options(FALSE),
      '#default_value' => isset($this->configuration['image_style']) ? $this->configuration['image_style'] : 'thumbnail',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $slack_token = $form_state->getValue('slack_token');
    $slack_channel = $form_state->getValue('slack_channel');

    $validation = \Drupal::service('slack_messages_block.messages_list_service')->validate($slack_token, $slack_channel);
    if ($validation === FALSE) {
      $form_state->setErrorByName('slack_token', t('Please set right token and channel'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['slack_token'] = $form_state->getValue('slack_token');
    $this->configuration['slack_channel'] = $form_state->getValue('slack_channel');
    $this->configuration['amount_messages'] = $form_state->getValue('amount_messages');
    $this->configuration['date_format'] = $form_state->getValue('date_format');
    $this->configuration['image_style'] = $form_state->getValue('image_style');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    //$slack_messages = slack_messages_block_get_messages($config['amount_messages'], $config['slack_token'], $config['slack_channel'], $config['date_format'], $config['image_style']);
    $slack_messages = $service = \Drupal::service('slack_messages_block.messages_list_service')->getMessages($config['slack_token'], $config['slack_channel'], $config['date_format'], $config['image_style'], $config['amount_messages']);

    return array(
      '#theme' => 'block--slack-messages-block',
      '#type' => 'markup',
      '#content' => $slack_messages,
      '#attached' => array(
        'library' => array(
          // Custom .css styles for block.
          'slack_messages_block/slack_messages_block',
        ),
      ),
    );
  }
}
