<?php

namespace Drupal\slack_messages_block;

use Drupal\Component\Serialization\Json;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Defines a service for building markup for slack messages.
 */
class MessagesListService {

  /**
   * Get slack channel name.
   *
   * @param string $slack_token
   *   Slack token.
   * @param string $slack_channel
   *   Name of slack channel.
   *
   * @return string $channel_id
   *   Channel id.
   */
  public function getChannel($slack_token = '', $slack_channel = '') {
    $data_channel = [];
    $url = "https://slack.com/api/channels.list?token=" . $slack_token . "&exclude_archived=" . $slack_token . "&pretty=1";
    $response = \Drupal::httpClient()->get($url, array('headers' => array('Accept' => 'text/plain')));
    $decoded_json = (string) $response->getBody();
    $data = Json::decode($decoded_json);

    if ($data['ok'] === FALSE && empty($data['channels'])) {
      return FALSE;
    }

    foreach ($data['channels'] as $number => $channel) {
      // Create an array with ids and names of all channels.
      if (isset($channel['id'])) {
        $data_channel[$channel['id']] = $channel['name'];

      }
    }
    // Get id of channel.
    $channel_id = array_search($slack_channel, $data_channel);


    return $channel_id;

  }

  /**
   * Get slack username of slack user who create message.
   */
  public function getAuthorMessages($token = '', $user = '') {
    $json = "https://slack.com/api/users.info?token=" . $token . "&user=" . $user . "&pretty=1";
    $response  = \Drupal::httpClient()->get($json, array('headers' => array('Accept' => 'text/plain')));
    $decoded_json = (string) $response->getBody();
    $data_user = Json::decode($decoded_json);

    return $data_user;
  }

  /**
   * Get picture of slack user who create message.
   */
  public function getAuthorPicture($url) {
    // Get user picture.
    $data_image = file_get_contents($url);
    $file_image = file_save_data($data_image);
    $file_image = File::load($file_image->id());
    if ($file_image) {
      $variables = array(
        'uri' => $file_image->getFileUri(),
      );
    }

    return $variables['uri'];
  }

  /**
   * @param string $slack_token
   *   Slack token.
   * @param string $slack_channel
   *   Name of slack channel.
   * @return bool
   *   Return TRUE if token and channel are correct.
   */
  public function validate($slack_token = '', $slack_channel = '') {
    $channel_id = $this->getChannel($slack_token, $slack_channel);
    $url = "https://slack.com/api/channels.info?token=" . $slack_token . "&channel=" . $channel_id . "&pretty=1";
    $response = \Drupal::httpClient()->get($url, array('headers' => array('Accept' => 'text/plain')));
    $decoded_json = (string) $response->getBody();
    $data = Json::decode($decoded_json);
    if ($data['error']) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Get list messages from channel. Returned array contains text of messaes,
   *   username and his/her picture, time of message.
   *
   * @param string $slack_token
   *   Slack token.
   * @param string $slack_channel
   *   Name of slack channel.
   * @param string $date_format
   *   Format of date|time.
   * @param string $image_style
   *   Image style.
   * @param int $amount_messages
   *   Amount of messages for display in block.
   *
   * @return array
   *   An array of messages.
   */
  public function getMessages($slack_token = '', $slack_channel = '', $date_format = '', $image_style = '', $amount_messages = NULL) {
    $output = [];
    $channel_id = $this->getChannel($slack_token, $slack_channel);

    //general
    //xoxp-69924528561-69924528897-69913624358-68fbf0d117

    $url = "https://slack.com/api/channels.history?token=" . $slack_token . "&channel=" . $channel_id . "&pretty=1";
    $response = \Drupal::httpClient()->get($url, array('headers' => array('Accept' => 'text/plain')));
    $decoded_json = (string) $response->getBody();
    $data = Json::decode($decoded_json);

    foreach ($data['messages'] as $number => $message) {
      $data_user = $this->getAuthorMessages($slack_token, $data['messages'][$number]['user']);
      // TODO If image is empty.
      $path = $this->getAuthorPicture($data_user['user']['profile']['image_512']);

      if (isset($number) &&  $amount_messages > $number && !empty($message)) {
        $time = \Drupal::service('date.formatter')->format($message['ts'], $date_format);

        $url_image = ImageStyle::load($image_style)->buildUrl($path);
        // Create array contains text of messages, usernames, picture and times.
        $output[] = [
          'time' => $time,
          'user' => $data_user['user']['name'],
          'text' => $message['text'],
          'image' => $url_image,
        ];
      }
    }

    return $output;
  }

}
